package vue;

import dao.AdherentDAO;
import dao.DemandeDAO;
import Util.JpaUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.model.Activite;
import metier.model.Adherent;
import metier.model.Demande;
import metier.model.Evenement;
import metier.model.EvenementGratuit;
import metier.model.EvenementPayant;
import metier.model.Lieu;
import metier.service.ServiceException;
import metier.service.ServiceMetier;

public class Main {

    /**
     * Méthode principale de l'application. Lance des tests sur les différentes
     * fonctionnalitées des services métiers
     *
     * @param args : non utilisé
     * @throws Exception : Exception pouvant être lancé par tout service et non
     * traitée.
     */
    public static void main(String args[]) throws Exception {
        JpaUtil.init();
        ServiceMetier s = new ServiceMetier();

        menuPrincipal(s);

        JpaUtil.destroy();
    }

    public static void menuPrincipal(ServiceMetier s) {
        boolean continuer = true;
        switch (afficherMenu()) {
            case 1:
                System.out.println(" == Ajouter adhérent == ");
                Adherent answer1 = new Adherent(demandeNoString("le nom"), demandeNoString("le prénom"), demandeNoString("l'e-mail"), demandeNoString("l'adresse"));
                try {
                    s.creerAdherent(answer1);
                } catch (ServiceException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("Un adhérent n'a pas été ajouté.");
                }
                System.out.println("Un adhérent a été ajouté.");
                break;
            case 2:
                System.out.println(" == Se connecter (vérifier si adhérent existe) == ");
                String emailTMP = demandeNoString("l'email de l'adhérent à trouver");
                Adherent answer2 = null;
                try {
                    answer2 = s.existeAdherent(emailTMP);
                } catch (ServiceException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (answer2.equals(null)) {
                    System.out.println("Aucun adhérent trouvé");
                } else {
                    System.out.println("Un adhérent a été trouvé");
                    System.out.println(answer2);
                }
                break;
            case 3:
                System.out.println(" == Ajouter demande == ");
                Adherent adherentTMP = choixAdherent(s);
                Date dateChoisieTMP = choixDate(s);
                Activite activiteChoisie = choixActivite(s);
                Demande demandeTMP = new Demande(dateChoisieTMP, "matin", false, adherentTMP, activiteChoisie);
                try {
                    s.creerDemande(demandeTMP);
                } catch (ServiceException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("Une demande n'a pas été ajouté.");
                }
                System.out.println("Une demande a été ajouté.");

                break;
            case 4:
                System.out.println(" == Lister adhérents == ");
                afficherListeAdherent(s);
                break;
            case 5:
                System.out.println(" == Lister demandes == ");
                afficherListeDemande(s);
                break;
            case 6:
                System.out.println(" == Lister demandes d'un adhérent == ");
                afficherListeDemandeDUnAdherent(s, choixAdherent(s));
                break;
            case 7:
                System.out.println(" == Lister évènements d'un adhérent == ");
                afficherListeEvenementDUnAdherent(s, choixAdherent(s));
                break;
            case 8:
                System.out.println(" == Lister evenements à administrer == ");
                afficherListeEvenements(s);
                break;
            case 9:
                System.out.println(" == Lister lieux == ");
                afficherListeLieux(s);
                break;
            case 10:
                System.out.println(" == Lister Activites == ");
                afficherListeActivite(s);
                break;
            case 11:
                System.out.println(" == Administrer un evenement == ");
                Evenement eventTMP = choixEvenement(s);
                administrerUnEvenement(s, eventTMP);
                break;
            case 12:
                System.out.println(" == Quitter programme == ");
                continuer = false;
                break;
        }

        if (continuer) {
            menuPrincipal(s);
        } else {
            System.out.println("Goodbye !");
        }
    }

    public static int afficherMenu() {
        System.out.println(" ==== MENU PRINCIPAL ==== ");
        int i = 1;
        System.out.println(i + " - Ajouter adhérent");
        i++;
        System.out.println(i + " - Se connecter (vérifier si adhérent existe)");
        i++;
        System.out.println(i + " - Ajouter demande");
        i++;
        System.out.println(i + " - Lister adhérents");
        i++;
        System.out.println(i + " - Lister demandes");
        i++;
        System.out.println(i + " - Lister demandes d'un adhérent");
        i++;
        System.out.println(i + " - Lister évènements d'un adhérent");
        i++;
        System.out.println(i + " - Lister evenements");
        i++;
        System.out.println(i + " - Lister lieux");
        i++;
        System.out.println(i + " - Lister Activites");
        i++;
        System.out.println(i + " - Administrer un evenement");
        i++;
        System.out.println(i + " - Quitter programme");
        i++;

        return demandeNombre(1, i);
    }

    public static int demandeNombre(int borneInfIncl, int borneSupIncl) {
        int answer = -1;
        Scanner sc = new Scanner(System.in);

        //Tant que l'entrée n'est pas valide.
        while (answer == -1) {
            System.out.println("Veuillez saisir un nombre : ");
            String str = sc.nextLine();
            System.out.print("Merci.");

            try {
                answer = Integer.parseInt(str);
            } catch (Exception e) {
                System.out.println(e);
            }
            if (borneInfIncl > answer || answer > borneSupIncl) {
                answer = -1;
                System.out.println("Entrée invalide, veuillez recommencer.");
            }
        }
        System.out.println("");

        return answer;
    }

    public static String demandeNoString(String pourQuelChamp) {
        Scanner sc = new Scanner(System.in);
        String answer = "";
        //Tant que l'entrée n'est pas valide.
        while (answer.length() == 0) {
            System.out.println("Veuillez saisir " + pourQuelChamp + " : ");
            answer = sc.nextLine();
            System.out.print("Merci.");

            if (answer.length() == 0) {
                System.out.println("Entrée invalide, veuillez recommencer.");
            }
        }
        System.out.println("");

        return answer;
    }

    public static Adherent choixAdherent(ServiceMetier s) {
        Adherent answer = null;
        List<Adherent> l = afficherListeAdherent(s);
        answer = l.get(demandeNombre(1, l.size()) - 1);
        System.out.println("DEBUG - Vous avez choisi :" + answer);

        return answer;
    }

    public static Activite choixActivite(ServiceMetier s) {
        Activite answer = null;
        List<Activite> l = afficherListeActivite(s);
        answer = l.get(demandeNombre(1, l.size()) - 1);
        //DEBUG//
        System.out.println("DEBUG - Vous avez choisi :" + answer);
        return answer;
    }

    public static Evenement choixEvenement(ServiceMetier s) {
        Evenement answer = null;
        List<Evenement> l = afficherListeEvenements(s);
        answer = l.get(demandeNombre(1, l.size()) - 1);
        //DEBUG//
        System.out.println("DEBUG - Vous avez choisi :" + answer);
        return answer;
    }

    public static Lieu choixLieu(ServiceMetier s) {
        Lieu answer = null;
        List<Lieu> l = afficherListeLieux(s);
        answer = l.get(demandeNombre(1, l.size()) - 1);
        //DEBUG//
        System.out.println("DEBUG - Vous avez choisi :" + answer);
        return answer;
    }

    public static Date choixDate(ServiceMetier s) {
        Date answer = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        while (answer == null) {
            try {
                answer = formatter.parse(demandeNoString("la date au format jj/mm/yyyy"));
            } catch (Exception e) {
                System.out.println(e);
                answer = null;
            }
        }
        return answer;
    }

    public static List<Adherent> afficherListeAdherent(ServiceMetier s) {
        List<Adherent> l = null;
        try {
            l = s.consulterListeAdherent();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("La liste d'adhérent n'a pas été correctement récupérée");

        }
        System.out.println("La liste d'adhérent a été correctement récupérée");

        int i = 1;
        for (Adherent a1 : l) {
            System.out.println(i + " - " + a1);
            i++;
        }

        //Renvoit la taille de la liste des adhérents
        return l;
    }

    public static List<Demande> afficherListeDemande(ServiceMetier s) {
        List<Demande> l = null;
        try {
            l = s.consulterListeDemande();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("La liste de demandes n'a pas été correctement récupérée");

        }
        System.out.println("La liste de demandes a été correctement récupérée");

        int i = 1;
        for (Demande a1 : l) {
            System.out.println(i + " - " + a1);
            i++;
        }

        //Renvoit la taille de la liste des adhérents
        return l;
    }

    public static List<Demande> afficherListeDemandeDUnAdherent(ServiceMetier s, Adherent a) {
        List<Demande> l = null;
        try {
            l = s.consulterListeDemande(a);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("La liste de demandes d'un adhérent n'a pas été correctement récupérée");

        }
        System.out.println("La liste de demandes d'un adhérent a été correctement récupérée");

        int i = 1;
        for (Demande a1 : l) {
            System.out.println(i + " - " + a1);
            i++;
        }

        //Renvoit la taille de la liste des adhérents
        return l;
    }

    public static List<Evenement> afficherListeEvenementDUnAdherent(ServiceMetier s, Adherent a) {
        List<Evenement> l = null;
        try {
            l = s.consulterListeEvenement(a);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("La liste d'évènements d'un adhérent n'a pas été correctement récupérée");

        }
        System.out.println("La liste d'évènements d'un adhérent a été correctement récupérée");

        int i = 1;
        for (Evenement e : l) {
            System.out.println(i + " - " + e);
            i++;
        }

        //Renvoit la taille de la liste des adhérents
        return l;
    }

    public static List<Activite> afficherListeActivite(ServiceMetier s) {
        List<Activite> l = null;
        try {
            l = s.consulterListeActivite();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("La liste d'activité n'a pas été correctement récupérée");

        }
        System.out.println("La liste d'activité a été correctement récupérée");

        int i = 1;
        for (Activite a1 : l) {
            System.out.println(i + " - " + a1);
            i++;
        }

        //Renvoit la taille de la liste des adhérents
        return l;
    }

    public static List<Evenement> afficherListeEvenements(ServiceMetier s) {
        List<Evenement> l = null;
        try {
            l = s.consulterListeEvenementAPlanifier();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("La liste d'événement n'a pas été correctement récupérée");

        }
        System.out.println("La liste d'événement a été correctement récupérée");

        int i = 1;
        for (Evenement a1 : l) {
            System.out.println(i + " - " + a1);
            i++;
        }

        //Renvoit la taille de la liste des adhérents
        return l;
    }

    public static List<Lieu> afficherListeLieux(ServiceMetier s) {
        List<Lieu> l = null;
        try {
            l = s.consulterListeLieu();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("La liste de lieux n'a pas été correctement récupérée");

        }
        System.out.println("La liste de lieux a été correctement récupérée");

        int i = 1;
        for (Lieu a1 : l) {
            System.out.println(i + " - " + a1);
            i++;
        }

        //Renvoit la taille de la liste des adhérents
        return l;
    }

    public static void administrerUnEvenement(ServiceMetier s, Evenement e) {
        if (e.getActivite().getPayant()) {
            //Si c'est payant
            try {
                s.administreEvenement((EvenementPayant) e, choixLieu(s), demandeNombre(0, 5000));
                System.out.println("Evenement payant correctement administré");

            } catch (ServiceException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Evenement payant pas correctement administré");
            }
        } else {
            try {
                //Si c'est gratuit
                s.administreEvenement((EvenementGratuit) e, choixLieu(s));
            } catch (ServiceException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Evenement gratuit correctement administré");

        }
    }
}
