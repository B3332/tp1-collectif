/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author vfalconier
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Evenement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    private boolean administre;
    private String momentJournee;
    @ManyToMany
    private List<Adherent> adherents;
    @ManyToOne
    private Activite activite;
    @ManyToOne
    private Lieu lieu;

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getMomentJournee() {
        return momentJournee;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isAdministre() {
        return administre;
    }

    public void setAdministre(boolean administre) {
        this.administre = administre;
    }

    public void setMomentJournee(String momentJournee) {
        this.momentJournee = momentJournee;
    }

    public Evenement(Date date, String momentJournee, List<Adherent> adherents, Activite activite) {
        this.date = date;
        this.momentJournee = momentJournee;
        this.adherents = adherents;
        this.activite = activite;
    }

    public Evenement() {
    }

    public List<Adherent> getAdherents() {
        return adherents;
    }

    public Activite getActivite() {
        return activite;
    }

    public Lieu getLieu() {
        return lieu;
    }

    public void setAdherents(List<Adherent> adherents) {
        this.adherents = adherents;
    }

    public void setActivite(Activite activite) {
        this.activite = activite;
    }

    public void setLieu(Lieu lieu) {
        this.lieu = lieu;
    }

    @Override
    public String toString() {
        String r = "Evenement{" + "id=" + id + ", date=" + date + ", administre=" + administre + ", momentJournee=" + momentJournee + ", adherents={";
        for(Adherent a:adherents){
            r += a +", ";
        }
        r += "}, activite=" + activite + ", lieu=" + lieu + '}';
        return r;
    }

}
