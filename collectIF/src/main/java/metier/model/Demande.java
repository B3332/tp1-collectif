/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author vfalconier
 */
@Entity
public class Demande implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Version
    private Long version; 
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;
    private String momentJournee;
    private boolean validation;
    @ManyToOne
    private Adherent adherent;
    @ManyToOne
    private Activite activite;

    public Demande() {
    }

    public Demande(Date date, String momentJournee, boolean validation, Adherent adherent, Activite activite) {
        this.date = date;
        this.momentJournee = momentJournee;
        this.validation = validation;
        this.adherent = adherent;
        this.activite = activite;
    }

    public Adherent getAdherent() {
        return adherent;
    }

    public Activite getActivite() {
        return activite;
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getMomentJournee() {
        return momentJournee;
    }

    public boolean getValidation() {
        return validation;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setMomentJournee(String momentJournee) {
        this.momentJournee = momentJournee;
    }

    public void setValidation(boolean validation) {
        this.validation = validation;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }

    public void setActivite(Activite activite) {
        this.activite = activite;
    }
    @Override
    public String toString() {
        return "Demande{" + "id=" + id + ", date=" + date + ", momentJournee=" + momentJournee + ", validation=" + validation + ", adherent=" + adherent + ", activite=" + activite + '}';
    }
}
