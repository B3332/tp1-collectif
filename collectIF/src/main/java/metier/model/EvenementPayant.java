/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;

/**
 *
 * @author vfalconier
 */
@Entity
public class EvenementPayant extends Evenement implements Serializable{
    private int Paf;

    public EvenementPayant() {
    }

    public EvenementPayant(Date date, String momentJournee, List<Adherent> adherents, Activite activite) {
        super(date, momentJournee, adherents, activite);
    }

    public int getPaf() {
        return Paf;
    }

    public void setPaf(int Paf) {
        this.Paf = Paf;
    }
    
}
