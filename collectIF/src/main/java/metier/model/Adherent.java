package metier.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Adherent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long numeroAdherent;
    private String nom;
    private String prenom;
    private String mail;
    private String adresse;
    private Double longitude;
    private Double latitude;
    @OneToMany(mappedBy = "adherent")
    private List<Demande> demande;
    @ManyToMany
    private List<Evenement> evenements;

    protected Adherent() {
    }

    public Adherent(String nom, String prenom, String mail, String adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.adresse = adresse;
        this.longitude = null;
        this.latitude = null;
        this.demande = new ArrayList<>();
        this.evenements = new ArrayList<>();
    }

    public List<Demande> getDemande() {
        return demande;
    }

    public List<Evenement> getEvenements() {
        return evenements;
    }

    public Long getId() {
        return numeroAdherent;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMail() {
        return mail;
    }

    public String getAdresse() {
        return adresse;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setDemande(List<Demande> demande) {
        this.demande = demande;
    }

    public void setEvenements(List<Evenement> evenements) {
        this.evenements = evenements;
    }

    @Override
    public String toString() {
        return "Adherent{" + "id=" + numeroAdherent + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", adresse=" + adresse + ", longitude=" + longitude + ", latitude=" + latitude + '}';
    }

    public void addDemande(Demande d) {
        if (!this.demande.contains(d)) {
            this.demande.add(d);
        }
    }
    
    public void addEvenement(Evenement e){
        if(!this.evenements.contains(e)){
            this.evenements.add(e);
        }
    }
}
