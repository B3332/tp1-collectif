package metier.service;

import com.google.maps.model.LatLng;
import dao.ActiviteDAO;
import dao.AdherentDAO;
import dao.DemandeDAO;
import dao.EvenementDAO;
import Util.JpaUtil;
import dao.LieuDAO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.RollbackException;
import metier.model.Activite;
import metier.model.Adherent;
import metier.model.Demande;
import metier.model.Evenement;
import metier.model.EvenementGratuit;
import metier.model.EvenementPayant;
import metier.model.Lieu;

/**
 * Ensemble de services donnés en accès aux utilisateurs de l'application.
 */
public class ServiceMetier {

    //-------------------------   METIER ADHERENT -------------------------
    /**
     * Crée un adhérent persistent, si possible. (Si n'existe pas déjà, si pas
     * erreur)
     *
     * @param a : l'adhérent à ajouter.
     * @throws ServiceException : Si l'adhérent existe déjà ou autre erreur
     * inconnue.
     */
    public void creerAdherent(Adherent a) throws ServiceException {
        if (existeAdherent(a.getMail()) == null) {
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            if (a.getLatitude() == null && a.getLongitude() == null) {
                LatLng coor = ServiceTechnique.getLatLng(a.getAdresse());
                a.setLatitude(coor.lat);
                a.setLongitude(coor.lng);
            }
            AdherentDAO aDAO = new AdherentDAO();
            aDAO.creer(a);
            try {
                JpaUtil.validerTransaction();
                ServiceTechnique.envoiEmailInscription(a, true);
            } catch (Exception e) {
                JpaUtil.annulerTransaction(); //On réouvrira une transaction
                ServiceTechnique.envoiEmailInscription(a, false);
                throw new ServiceException("ERREUR : Adhérent n'a pas pu être ajouté.");
            } finally {
                JpaUtil.fermerEntityManager();
            }
        } else {
            ServiceTechnique.envoiEmailInscription(a, false);
            throw new ServiceException("ERREUR : Adhérent déjà existant.");
        }
    }

    /**
     * Renvoit la liste complète des adhérents, sans distinction.
     *
     * @return la liste complète des adhérents.
     * @throws ServiceException : impossible de consulter la liste des adhérents
     * (erreur en lecture)
     */
    public List<Adherent> consulterListeAdherent() throws ServiceException {
        JpaUtil.creerEntityManager();
        AdherentDAO aDAO = new AdherentDAO();
        List<Adherent> liste = null;
        try {
            liste = aDAO.findAll();
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des adhérents.");
        } finally {
            JpaUtil.fermerEntityManager();
        }
        return liste;
    }

    /**
     * Vérifie si un adhérent est présent dans la base ou non.
     *
     * @param mail : l'e-mail de l'adhérent à vérifier.
     * @return : null si l'adhérent n'existe pas, l'adhérent en question si il
     * existe.
     * @throws ServiceException : impossible de consulter la liste des adhérents
     * (erreur en lecture)
     */
    public Adherent existeAdherent(String mail) throws ServiceException {
        JpaUtil.creerEntityManager();
        AdherentDAO aDAO = new AdherentDAO();
        Adherent result = null;
        try {
            List<Adherent> a = aDAO.findByMail(mail);
            if (!a.isEmpty()) {
                result = a.get(0);
            }
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des adhérents pour effectuer la recherche.");
        } finally {
            JpaUtil.fermerEntityManager();
        }
        return result;
    }

    //-------------------------   METIER ACTIVITE -------------------------
    /**
     * Renvoit la liste complète des activités, sans distinction.
     *
     * @return la liste complète
     * @throws ServiceException : impossible de consulter la liste des activites
     * (erreur en lecture)
     */
    public List<Activite> consulterListeActivite() throws Exception {
        //Try Catch pour les RollBack
        JpaUtil.creerEntityManager();
        ActiviteDAO aDAO = new ActiviteDAO();
        List<Activite> liste = null;
        try {
            liste = aDAO.findAll();
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des activite.");
        } finally {
            JpaUtil.fermerEntityManager();
        }
        return liste;
    }

    //-------------------------   METIER LIEU -------------------------
    /**
     * Renvoit la liste complète des lieux, sans distinction.
     *
     * @return la liste complète
     * @throws ServiceException : impossible de consulter la liste des activites
     * (erreur en lecture)
     */
    public List<Lieu> consulterListeLieu() throws ServiceException {
        JpaUtil.creerEntityManager();
        LieuDAO lDAO = new LieuDAO();
        List<Lieu> liste = null;
        try {
            liste = lDAO.findAll();
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des lieux.");
        } finally {
            JpaUtil.fermerEntityManager();
        }
        return liste;
    }

    //-------------------------   METIER DEMANDE -------------------------
    /**
     * Permet de stocker une demande (la rend persistante). L'ajoute également à
     * l'utilisateur qui a fait la demande.
     *
     * @param d : la demande a stocker.
     * @throws ServiceException : si la demande existe déjà, collision entre des
     * demandes simultanées, problème durant l'ajout
     */
    public void creerDemande(Demande d) throws ServiceException {
        JpaUtil.creerEntityManager();

        try { //On encapsule tout en cas d'erreur non gérée.
            DemandeDAO dDAO = new DemandeDAO();

            //On vérifie l'existance de la demande pour un adhérent
            if (dDAO.exist(d)) {
                throw new ServiceException("ERREUR : La demande existe déjà pour l'adhérent.");
            } else {
                //On créer la demande dans une transaction isolée
                JpaUtil.ouvrirTransaction();
                //On créé la demande
                dDAO.creer(d);
                //On l'ajoute du côté de l'adhérent
                Adherent current = d.getAdherent();
                current.addDemande(d);
                //On met juste à jour l'adhérent
                AdherentDAO aDAO = new AdherentDAO();
                current = aDAO.update(current);
                //On valide la transaction
                try {
                    JpaUtil.validerTransaction();
                } catch (RollbackException e) {
                    JpaUtil.annulerTransaction(); //On réouvrira une transaction
                    throw new ServiceException("ERREUR : problème durant l'ajout de la demande. Veuillez re-essayer.");
                }
            }

            boolean isInCollisionError = false; //Pour l'instant il n'y a pas d'erreur

            do {
                //On cherche les demandes existantes de mêmes caractéristiques
                List<Demande> demandesExistantes = dDAO.findSame(d);

                if (!demandesExistantes.isEmpty()) { //Si il existe des demandes
                    //On créer la demande dans une transaction isolée
                    JpaUtil.ouvrirTransaction();

                    //On recupère le nombre de participants de l'activité
                    int nbPersonne = d.getActivite().getNbParticipants();
                    //DEBUG // System.err.println(nbPersonne);
                    //DEBUG // System.err.println(demandesExistantes.size());

                    if (demandesExistantes.size() == nbPersonne) { //Si le seuil de personne est atteint

                        //On valide chaque demande et on recupère chaque adhérent dans une liste
                        List<Adherent> listAd = new ArrayList<>();
                        for (Demande i : demandesExistantes) {
                            i.setValidation(true);
                            listAd.add(i.getAdherent());
                        }

                        // On merge dans la base de donnees
                        dDAO.updateAll(demandesExistantes);

                        //On cree l'évènement avec les caractéristiques de la demandeExistante + payant/pas payant
                        Evenement e;
                        if (d.getActivite().getPayant()) {
                            e = new EvenementPayant(d.getDate(), d.getMomentJournee(), listAd, d.getActivite());
                        } else {
                            e = new EvenementGratuit(d.getDate(), d.getMomentJournee(), listAd, d.getActivite());
                        }

                        //On créé la DAO d'évenement
                        EvenementDAO eDAO = new EvenementDAO();
                        eDAO.creer(e);

                        //On ajoute l'évenement à la liste des evenements de chaque adhérent
                        for (Adherent a : listAd) {
                            a.addEvenement(e);
                        }

                        //On enregistre les adhérents
                        AdherentDAO aDAO = new AdherentDAO();
                        aDAO.updateAll(listAd);
                    }
                    try { //On valide la transaction
                        JpaUtil.validerTransaction();
                        isInCollisionError = false; //Si il n'y a pas eu d'erreur
                    } catch (RollbackException e) {
                        JpaUtil.annulerTransaction(); //On réouvrira une transaction
                        isInCollisionError = true; //Si il y a une une erreur, on bouclera.
                        throw new ServiceException("ERREUR : Collision entre des demandes simultanées. La demande sera réitérée automatiquement. (OptimisticLockException)");
                    }
                }
            } while (isInCollisionError == true);

        } catch (ServiceException e) {
            JpaUtil.annulerTransaction();
            throw e;
        } finally {
            JpaUtil.fermerEntityManager();
        }

    }

    /**
     * Permet de connaître les demandes en cours.
     *
     * @return la liste des demandes
     * @throws ServiceException : impossible de consulter la liste des activites
     * (erreur en lecture)
     */
    public List<Demande> consulterListeDemande() throws ServiceException {
        //Try Catch pour les RollBack
        JpaUtil.creerEntityManager();
        DemandeDAO dDAO = new DemandeDAO();
        List<Demande> liste = null;
        try {
            liste = dDAO.findAll();
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des demandes.");
        } finally {
            JpaUtil.fermerEntityManager();
        }
        return liste;
    }

    /**
     * Permet de connaître les demandes d'un adherent
     *
     * @param a : l'adhérent dont on veut connaître les demandes.
     * @return les demandes d'un adhérent
     * @throws ServiceException : impossible de consulter la liste des activites
     * (erreur en lecture)
     */
    public List<Demande> consulterListeDemande(Adherent a) throws ServiceException {
        List<Demande> answer = null;
        try {
            answer = a.getDemande();
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des demandes d'un adhérent.");
        }
        return answer;
    }

    /**
     * Permet de connaître les demandes à valider (facultatif)
     *
     * @param d la demande
     * @return la liste des demandes à valider
     * @throws ServiceException : Erreur à la consultation du nombre de
     * demandes.
     */
    public int compterDemande(Demande d) throws ServiceException {
        // TODO // Peut être à modifier, pourquoi il y a un argument à la fonction ? 
        JpaUtil.creerEntityManager();
        DemandeDAO dDAO = new DemandeDAO();
        List<Demande> liste = null;
        try {
            liste = dDAO.findAll();
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des demandes (comptage).");
        } finally {
            JpaUtil.fermerEntityManager();
        }

        int answer = 0;
        //On évite un déréferecenement null de pointeur
        if (liste != null) {
            for (int i = 0; i < liste.size(); i++) {
                if (liste.get(i).getValidation() == false) {
                    answer++;
                }
            }
        }

        return answer;
    }

    //-------------------------   METIER EVENEMENT -------------------------
    /**
     * Permet de connaître la liste des événements auquel participe et a
     * participé un adhérents
     *
     * @param a : l'adhérent dont on veut connaître les evenements liés
     * @return : la liste des evenements dans lesquels l'adhérent est impliqué
     * @throws ServiceException : si erreur à la consultation (lecture)
     */
    public List<Evenement> consulterListeEvenement(Adherent a) throws ServiceException {
        List<Evenement> answer = null;
        try {
            answer = a.getEvenements();
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible de consulter la liste des evenements d'un adherent.");
        }

        return answer;
    }

    /**
     * Permet de connaître les demandes à valider par l’administrateur.
     *
     * @return : la liste des evenements à planifier
     * @throws ServiceException : si erreur à la consultation (lecture)
     */
    public List<Evenement> consulterListeEvenementAPlanifier() throws ServiceException {
        //NOTE : doute quant à l'utilité de la transaction.
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        EvenementDAO eDAO = new EvenementDAO();
        List<Evenement> liste = null;

        try {
            liste = eDAO.findAValider();
            JpaUtil.validerTransaction();
        } catch (Exception e) {
            JpaUtil.annulerTransaction();
            throw new ServiceException("ERREUR : Impossible de consulter la liste des evenements à planifier.");
        } finally {
            JpaUtil.fermerEntityManager();
        }
        return liste;
    }

    /**
     * Ajouter un lieu à un événement gratuit qui n’a pas encore été administré.
     *
     * @param e : l'evenement à administrer
     * @param l : le lieu à affecter à l'évenement.
     * @return : l'evenement une fois administré.
     * @throws ServiceException : si erreur à la modification (écriture)
     */
    public Evenement administreEvenement(EvenementGratuit e, Lieu l) throws ServiceException {
        //On renseigne les informations
        e.setAdministre(true);
        e.setLieu(l);

        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        EvenementDAO eDAO = new EvenementDAO();

        try {
            e = (EvenementGratuit) eDAO.update(e);
            JpaUtil.validerTransaction();
            ServiceTechnique.envoiEMailEvenement(e);
        } catch (Exception exce) {
            JpaUtil.annulerTransaction();
            throw new ServiceException("ERREUR : Impossible de modifier l'evenement gratuit.");
        } finally {
            JpaUtil.fermerEntityManager();
        }

        return e;
    }

    /**
     * Ajouter un lieu et un prix à un événement payant qui n’a pas encore été
     * administré.
     *
     * @param e : l'evenement à administrer
     * @param l : le lieu à affecter à l'évenement.
     * @param paf : le prix à affecter à l'évenement.
     * @return : l'evenement une fois administré.
     * @throws ServiceException : si le prix est inférieur ou egal 0 ou si erreur à la
     * modification (écriture)
     */
    public Evenement administreEvenement(EvenementPayant e, Lieu l, int paf) throws ServiceException {
        if (paf <= 0) {
            throw new ServiceException("Evènement payant, impossible de fixer une PAF nulle ou négative");
        } else {
            // On ajoute le lieu et la PAF à l'évènement
            e.setPaf(paf);
            e.setLieu(l);
            e.setAdministre(true);

            //On merge dans la bdd
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();

            try {
                EvenementDAO eDAO = new EvenementDAO();
                e = (EvenementPayant) eDAO.update(e);
                JpaUtil.validerTransaction();
                ServiceTechnique.envoiEMailEvenement(e);
            } catch (Exception exce) {
                JpaUtil.annulerTransaction();
                throw new ServiceException("ERREUR : Impossible de modifier l'evenement payant.");
            } finally {
                JpaUtil.fermerEntityManager();
            }

        }
        return e;
    }
}
