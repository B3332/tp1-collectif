package metier.service;

import Util.GeoTest;
import com.google.maps.*;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.model.Adherent;
import metier.model.Evenement;
import metier.model.Lieu;

/**
 * Services qui permettent des actions techniques ne dépendants pas du système
 * interne (envoi email, etc.)
 */
public class ServiceTechnique {

    /**
     * Envoi un e-mail à l'adhérent (simulé par envoi dans console)
     *
     * @param a : l'adhérent qui recevra le mail
     * @param objet : entête du message
     * @param message : message à ajouter (corps de l'e-mail)
     */
    public static void envoiEMail(Adherent a, String objet, String message) {
        System.out.println("==> Simulation d'envoie Email à: " + a.getMail() + " <=="); // Le bon contenu du mail :)

        String reponse = "Expéditeur : " + "collectif@collectif.org" + "\n";
        reponse += "Pour : " + a.getMail() + "\n";
        reponse += "Sujet : " + objet + "\n";
        reponse += message;
        System.out.println(reponse);
    }

    /**
     * Envoi un e-mail à l'adhérent pour son inscription(simulé par envoi dans
     * console)
     *
     * @param a : l'adhérent qui recevra le mail
     * @param inscriptionReussie : permet de savoir quel message envoyer par
     * email à l'adhérent
     * @throws ServiceException : erreur à l'envoi de l'email
     */
    public static void envoiEmailInscription(Adherent a, boolean inscriptionReussie) throws ServiceException {
        try {
            String reponse = "Corps : " + " Bonjour " + a.getPrenom() + ",\n";
            if (inscriptionReussie) {
                reponse += "Nous vous confirmons votre adhésion à l'association COLLECT'IF. Votre numéro d'adhérent est : " + a.getId() + ".\n";
            } else {
                reponse += "Votre adhésion à l'association COLLECT'IF a malencontreusement échouée ... Merci de recommencer utlérieurement.\n";
            }

            envoiEMail(a, "Bienvenue chez Collect'IF", reponse);
        } catch (Exception e) {
            throw new ServiceException("ERREUR : email d'inscription n'a pas pu être envoyé.");
        }
    }

    /**
     * Envoi un e-mail type pour la création d'un événement.
     *
     * @param e : l'événement en question, chaque participant recevra un e-mail.
     * @throws ServiceException : erreur à l'envoi de l'email
     */
    public static void envoiEMailEvenement(Evenement e) throws ServiceException {
        try {
            List<Adherent> l = e.getAdherents();
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
            for (Adherent a : l) {
                String reponse = "Corps : " + " Bonjour " + a.getPrenom() + ",\n";
                reponse += "Comme vous l'aviez souhaité, COLLECT'IF organise un événement de " + e.getActivite().getDenomination() + " le " + formatter.format(e.getDate()) + ". Vous trouverez ci-dessous les détails de cet événement." + "\n";
                reponse += "    Associativement vôtre," + "\n";
                reponse += "        Le Responsable de l'Association" + "\n";
                reponse += "\n";

                reponse += "Evénement : " + e.getActivite().getDenomination() + "\n";
                reponse += "Date : " + formatter.format(e.getDate()) + "\n";
                reponse += "Lieu : " + e.getLieu().toStringNice() + "\n";
                try {
                    reponse += "(à " + calculDistance(a, e.getLieu()) + "km de chez vous)\n";
                } catch (Exception ex) {
                    Logger.getLogger(ServiceTechnique.class.getName()).log(Level.SEVERE, null, ex);
                }
                reponse += "\n";

                reponse += "Vous jouerez avec : \n";
                for (Adherent autres : l) {
                    if (!a.equals(autres)) {
                        reponse += autres.getPrenom() + " " + autres.getNom() + "\n";
                    }
                }
                envoiEMail(a, "Nouvel Evènement Collect'IF", reponse);
            }
        } catch (Exception exce) {
            throw new ServiceException("ERREUR : email de l'evenement n'a pas pu être envoyé.");
        }

    }

    /**
     * Récupère la latitude et la latitude à partir d’une adresse
     *
     * @param adresse : dont on veut les coordonnées GPS.
     * @return : un objet LatLng qui correspond à un contenaire de coordonées
     * GPS. Null si pas de coordonnées
     * @throws ServiceException : si il a été impossible de les récupérer.
     */
    public static LatLng getLatLng(String adresse) throws ServiceException {
        LatLng answer = null;
        try {
            answer = GeoTest.getLatLng(adresse);
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible d'obtenir les coordonées GPS.");
        }
        return answer;
    }

    public static double calculDistance(Adherent a, Lieu l) throws ServiceException {
        double answer = 0.0;
        try {
            LatLng orign = new LatLng(a.getLatitude(), a.getLongitude());
            LatLng destination = new LatLng(l.getLatitude(), l.getLongitude());
            answer = GeoTest.getFlightDistanceInKm(orign, destination);
        } catch (Exception e) {
            throw new ServiceException("ERREUR : Impossible d'obtenir la distance GPS.");
        }

        return answer;
    }
}
