package metier.service;

/**
 * Classe permettant de spécifier les erreurs lancées par la couche Service
 */
public class ServiceException extends Exception{

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
