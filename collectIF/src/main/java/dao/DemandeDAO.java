/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.model.Demande;

/**
 *
 * @author vfalconier
 */
public class DemandeDAO {

    public void creer(Demande d) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(d);
    }

    public Demande findById(long id) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        Demande demande = null;
        try {
            demande = em.find(Demande.class, id);
        } catch (Exception e) {
            throw e;
        }
        return demande;
    }

    public List<Demande> findAll() throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Demande> demande = null;
        try {
            Query q = em.createQuery("SELECT a FROM Demande a");
            demande = (List<Demande>) q.getResultList();
        } catch (Exception e) {
            throw e;
        }

        return demande;
    }

    public List<Demande> findSame(Demande d) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Demande> dReturn = null;
        try {
            Query q = em.createQuery("SELECT d FROM Demande d WHERE d.date = :date AND d.momentJournee = :momentJournee AND d.activite = :activite AND d.validation = :validation");
            q.setParameter("date", d.getDate());
            q.setParameter("momentJournee", d.getMomentJournee());
            q.setParameter("activite", d.getActivite());
            q.setParameter("validation", d.getValidation());
            dReturn = (List<Demande>) q.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return dReturn;
    }

    public List<Demande> updateAll(List<Demande> demandesExistantes) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        for (Demande d : demandesExistantes) {
            d = em.merge(d);
        }
        return demandesExistantes;
    }

    public boolean exist(Demande d) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Demande> dReturn = null;
        try {
            Query q = em.createQuery("SELECT d FROM Demande d WHERE d.adherent = :adherent AND d.date = :date AND d.momentJournee = :momentJournee AND d.activite = :activite AND d.validation = :validation");
            q.setParameter("adherent", d.getAdherent());
            q.setParameter("date", d.getDate());
            q.setParameter("momentJournee", d.getMomentJournee());
            q.setParameter("activite", d.getActivite());
            q.setParameter("validation", d.getValidation());
            dReturn = (List<Demande>) q.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return !dReturn.isEmpty();
    }
}
