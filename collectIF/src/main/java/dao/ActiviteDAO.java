package dao;

import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.model.Activite;

public class ActiviteDAO {

    public void creer(Activite a) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(a);
        //TODO // THROW EXCEPTION SI PAS DE EM ! 
    }

    public Activite findById(long id) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        Activite activite = null;
        try {
            activite = em.find(Activite.class, id);
        } catch (Exception e) {
            throw e;
        }
        return activite;
    }

    public List<Activite> findAll() throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Activite> activites = null;
        try {
            Query q = em.createQuery("SELECT a FROM Activite a");
            activites = (List<Activite>) q.getResultList();
        } catch (Exception e) {
            throw e;
        }

        return activites;
    }
}
