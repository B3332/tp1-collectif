package dao;

import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.model.Adherent;

public class AdherentDAO {

    public void creer(Adherent a) {
        //AJOUTER LES COORODNES A PARTIR de L'ADRESSE
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(a);
    }

    public Adherent findById(long id) throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        Adherent adherent = null;
        try {
            adherent = em.find(Adherent.class, id);
        } catch (Exception e) {
            throw e;
        }
        return adherent;
    }

    public List<Adherent> findAll() throws Exception {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Adherent> adherents = null;
        try {
            Query q = em.createQuery("SELECT a FROM Adherent a");
            adherents = (List<Adherent>) q.getResultList();
        } catch (Exception e) {
            throw e;
        }

        return adherents;
    }

    public List<Adherent> findByMail(String mail) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Adherent> a = null;
        try {
            Query q = em.createQuery("SELECT a FROM Adherent a WHERE a.mail = :mail");
            q.setParameter("mail", mail);
            a = (List<Adherent>) q.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return a;
    }

    public List<Adherent> updateAll(List<Adherent> listAd) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        for (Adherent a : listAd) {
            a = em.merge(a);
        }
        return listAd;
    }

    public Adherent update(Adherent a) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        return em.merge(a);
    }
}
