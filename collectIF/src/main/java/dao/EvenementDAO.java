/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Util.JpaUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import metier.model.Evenement;

/**
 *
 * @author vfalconier
 */
public class EvenementDAO {

    public void creer(Evenement e) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(e);
    }

    public List<Evenement> findAValider() {
        EntityManager em = JpaUtil.obtenirEntityManager();
        List<Evenement> e = null;
        try {
            Query q = em.createQuery("SELECT e FROM Evenement e WHERE e.administre = false");
            e = (List<Evenement>) q.getResultList();
        } catch (Exception err) {
            throw err;
        }
        return e;
    }

    public Evenement update(Evenement e) {
        EntityManager em = JpaUtil.obtenirEntityManager();
        return em.merge(e);
    }
    
}
