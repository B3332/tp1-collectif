/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import dao.ActiviteDAO;
import dao.AdherentDAO;
import dao.EvenementDAO;
import Util.JpaUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import metier.model.Activite;
import metier.model.Adherent;
import metier.model.Demande;
import metier.model.Evenement;
import metier.model.Lieu;
import metier.service.ServiceException;
import metier.service.ServiceMetier;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vincent Falconieri
 */
public class ServiceTest {

    List<Adherent> adherentsTest;
    List<Activite> activiteTest;
    List<Demande> demandeTest;
    List<Evenement> evenementTest;
    int remplissage;
    ServiceMetier s;

    public ServiceTest() {

        //Instanciation des listes / attributs de classe
        adherentsTest = new ArrayList<>();
        activiteTest = new ArrayList<>();
        demandeTest = new ArrayList<>();
        evenementTest = new ArrayList<>();
        s = new ServiceMetier();
        this.remplissage = 5;

        //Pour générer des dates
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        //On genère des jeux de tests
        for (int i = 0; i < remplissage; i++) {
            adherentsTest.add(new Adherent("Coco " + i, "Giraud " + i, "coco" + i + "@insa-lyon.com", i + " rue du Lac"));
            activiteTest.add(new Activite("activite " + i, Boolean.FALSE, i));
            //Génération de date
            String dateGenerated = "0" + i + "/06/2013";
            Date dTMP = null;
            try {
                dTMP = formatter.parse(dateGenerated);
            } catch (ParseException ex) {
                Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            demandeTest.add(new Demande(dTMP, "matin", false, adherentsTest.get(i), activiteTest.get(i)));
            //evenementTest.add(new Evenement(dTMP, "matin", adherentsTest.subList(i, i), activiteTest.get(i)));
        }

        // DEBUG // Affichage des listes crées.
        /*
        System.out.println(adherentsTest);
        System.out.println(activiteTest);
        System.out.println(demandeTest);
        System.out.println(evenementTest);
        //*/
    }

    @BeforeClass
    public static void setUpClass() {
        JpaUtil.init();
    }

    @AfterClass
    public static void tearDownClass() {
        JpaUtil.destroy();

    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void helloItsWorking() {
        //open transaction
        //ajouter element

        assertTrue(true);
        //AssertFalse
        //assertEquals (objet à la main, calculé) = (attendu,calculé)
        //asseertNotEquals
        //assertSame = avec double égale (reference)
    }

    //-------------------------  ADHERENT -------------------------
    @Test
    public void ajoutSimpleAdherentTest() {
        try {
            s.creerAdherent(adherentsTest.get(0));
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    @Test
    public void consulterListeAdherentTest() {
        //Note : on devrait flush pour savoir le nombre exact et faire un vrai test.
        List<Adherent> a = null;
        try {
            a = s.consulterListeAdherent();
        } catch (Exception ex) {
            System.err.println(ex);
        }

        //Vérifier si on renvoit une liste avec quelques chose dedans.
        assertFalse(a.isEmpty());
    }

    @Test
    public void existeAdherentTest() {
        //Note : fonctionne parce que adhérent ajouté au premier test.
        //Renvoi null si inexistant
        String mail = adherentsTest.get(0).getMail();
        Adherent answer = null;
        try {
            answer = s.existeAdherent(mail);
        } catch (ServiceException ex) {
            Logger.getLogger(ServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertNotNull(answer);
    }

    @Test
    public void adherentCreerResistanceDoublonTest() {
        boolean answer = false;
        try {
            s.creerAdherent(adherentsTest.get(0));
            s.creerAdherent(adherentsTest.get(0));
        } catch (ServiceException exce) {
            answer = true;
        }
        assertTrue(answer);
    }
    //-------------------------  ACTIVITE -------------------------

    @Test
    public void consulterListeActiviteTest() {
        //Note : on devrait flush pour savoir le nombre exact et faire un vrai test.
        List<Activite> a = null;
        try {
            a = s.consulterListeActivite();
        } catch (Exception ex) {
            System.err.println(ex);
        }

        //Vérifier si on renvoit une liste avec quelques chose dedans.
        assertFalse(a.isEmpty());
    }

    //-------------------------  LIEU -------------------------
    @Test
    public void consulterListeLieuTest() {
        //Note : on devrait flush pour savoir le nombre exact et faire un vrai test.
        List<Lieu> a = null;
        try {
            a = s.consulterListeLieu();
        } catch (Exception ex) {
            System.err.println(ex);
        }

        //Vérifier si on renvoit une liste avec quelques chose dedans.
        assertFalse(a.isEmpty());
    }

    //-------------------------  DEMANDE -------------------------
    @Test
    public void creerDemandeTest() {
        //Nécessaire pour éviter erreur
        try {
            //Ajout d'activite à la main.
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            ActiviteDAO acDAO = new ActiviteDAO();
            acDAO.creer(activiteTest.get(1));
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();

            //Ajout adhérent
            s.creerAdherent(adherentsTest.get(1));
        } catch (ServiceException ex) {
            System.err.println(ex);
        }

        try {
            s.creerDemande(demandeTest.get(1));
        } catch (ServiceException ex) {
            System.err.println(ex);
        }
    }

    @Test
    public void consulterListeDemandeTest() {
        List<Demande> a = null;
        try {
            a = s.consulterListeDemande();
        } catch (Exception ex) {
            System.err.println(ex);
        }
        // DEBUG // System.out.println(a);

        //Vérifier si on renvoit une liste avec quelques chose dedans.
        //assertFalse(a.isEmpty());
    }

    @Test
    public void consulterListeDemandeAdherentTest() {
        //==> Création des données

        //Paramèrtres & pour générer des dates
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        int nbDemandes = 5;

        //Ajoute 1 adhérent
        Adherent adhTest = new Adherent("Coco50", "Giraud50", "coco50@insa-lyon.com", "50 rue du Lac");
        try {
            s.creerAdherent(adhTest);
        } catch (ServiceException ex) {
            System.err.println(ex);
        }
        //Ajout d'activite à la main.

        ActiviteDAO acDAO = new ActiviteDAO();

        //Ajout des demandes pour un adhérent
        for (int i = 0; i < nbDemandes; i++) {
            //Génération de date
            String dateGenerated = "0" + i + "/06/2013";
            Date dTMP = null;
            //Tentative de générer la date à partir du texte
            try {
                dTMP = formatter.parse(dateGenerated);
            } catch (Exception ex) {
                System.err.println(ex);
            }
            //Tentative d'ajouter l'activité correspondante
            JpaUtil.creerEntityManager();
            JpaUtil.ouvrirTransaction();
            acDAO.creer(activiteTest.get(i));
            JpaUtil.validerTransaction();
            JpaUtil.fermerEntityManager();

            //Tentative d'ajouter la demande
            try {
                s.creerDemande(new Demande(dTMP, "matin", true, adhTest, activiteTest.get(i)));
            } catch (Exception ex) {
                System.err.println(ex);
            }
        }

        //Récupération de la liste des demandes
        List<Demande> answer = null;
        try {
            answer = s.consulterListeDemande(adhTest);
        } catch (Exception ex) {
            System.err.println(ex);
        }
        //DEBUG // System.out.println(answer);

        //Vérifier si on renvoit une liste avec quelques chose dedans et le bon nombre
        assertFalse(answer.isEmpty() && answer.size() == nbDemandes);
    }

    @Test
    public void compterDemandeTest() throws Exception {
        //NOTE : ATTENTION ! 
        // On fait l'hypothèse que toutes les demandes ajoutées jusqu'à mainteant n'ont pas été validées ! 
        List<Demande> a = null;
        try {
            a = s.consulterListeDemande();
        } catch (Exception ex) {
            System.err.println(ex);
        }

        //null car la fonction n'en aurait pas besoin ? 
        assertTrue(a.size() >= s.compterDemande(null));
    }
    //-------------------------  EVENEMENT -------------------------

    @Test
    public void creerEvenementTest() {
        //On cree l'évènement avec les caractéristiques de la demandeExistante
        List<Adherent> listAd = adherentsTest.subList(0, 1);
        Demande d = demandeTest.get(0);
        //Evenement e = new Evenement(d.getDate(), d.getMomentJournee(), listAd, d.getActivite());

        ActiviteDAO acDAO = new ActiviteDAO();
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        acDAO.creer(activiteTest.get(0));
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();

        AdherentDAO adDAO = new AdherentDAO();
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        adDAO.creer(adherentsTest.get(0));
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();

        EvenementDAO eDAO = new EvenementDAO();
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        //eDAO.creer(e);
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();

        //System.out.println(e);
    }

    @Test
    public void creerPuisconsulterListeEvenementAPlanifierTest() {
        //On ajoute des évenements à administrer.
        List<Evenement> result = null;
        try {
            result = s.consulterListeEvenementAPlanifier();
        } catch (ServiceException ex) {
            Logger.getLogger(ServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        //DEBUG// System.err.println(result);
        assertTrue(!result.equals(null) && result.size() >= 0);
    }

    @Test
    public void administreEvenementTest() {
//Evenement Evenement e, boolean admin, int paf
    }
    
    @Test
    public void envoiEmailInscriptionTest() {
//Evenement Evenement e, boolean admin, int paf
    }
    
    @Test
    public void envoiEMailEvenementTest() {
//Evenement Evenement e, boolean admin, int paf
    }

}
