
import dao.ActiviteDAO;
import dao.AdherentDAO;
import dao.DemandeDAO;
import Util.JpaUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import metier.model.Activite;
import metier.model.Adherent;
import metier.model.Demande;
import metier.model.Evenement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vincent Falconieri
 */
public class DAOTest {

    List<Adherent> adherentsTest;
    List<Activite> activiteTest;
    List<Demande> demandeTest;
    List<Evenement> evenementTest;
    int remplissage;

    public DAOTest() {
        this.remplissage = 5;
        //Instanciation des listes
        adherentsTest = new ArrayList<>();
        activiteTest = new ArrayList<>();
        demandeTest = new ArrayList<>();
        evenementTest = new ArrayList<>();

        //Pour générer des dates
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        for (int i = 0; i < remplissage; i++) { //On genère des jeux de tests
            adherentsTest.add(new Adherent("Coco " + i, "Giraud " + i, "coco" + i + "@insa-lyon.com", i + " rue du Lac"));
            activiteTest.add(new Activite("activite " + i, Boolean.FALSE, i));
            //Génération de date
            String dateGenerated = "0" + i + "/06/2013";
            Date dTMP = null;
            try {
                dTMP = formatter.parse(dateGenerated);
            } catch (ParseException ex) {
                Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            demandeTest.add(new Demande(dTMP, "matin", false, adherentsTest.get(i), activiteTest.get(i)));
            //evenementTest.add(new Evenement(dTMP, "matin", adherentsTest, activiteTest.get(i)));
        }

        // DEBUG //
        /*
        System.out.println(adherentsTest);
        System.out.println(activiteTest);
        System.out.println(demandeTest);
        System.out.println(evenementTest);
        //*/
    }

    @BeforeClass
    public static void setUpClass() {
        //Create entity manager
        JpaUtil.init();
    }

    @AfterClass
    public static void tearDownClass() {
        //Destroy entity mangaer
        JpaUtil.destroy();
    }

    @Before //avant chaque méthode
    public void setUp() {

        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
    }

    @After //après chaque méthode
    public void tearDown() {
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void helloItsWorking() {
        //open transaction
        //ajouter element

        assertTrue(true);
        //AssertFalse
        //assertEquals (objet à la main, calculé) = (attendu,calculé)
        //asseertNotEquals
        //assertSame = avec double égale (reference)
    }

    //------------------------- ACTIVITE -------------------------
    @Test
    public void activiteFindByIDTest() {
        //Ajout
        int no = 1;
        EntityManager em = JpaUtil.obtenirEntityManagerTest();
        em.persist(activiteTest.get(no));
        //Recherche
        ActiviteDAO aDAO = new ActiviteDAO();
        Activite result = null;
        try {
            result = aDAO.findById(activiteTest.get(no).getId());
        } catch (Exception ex) {
            Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertEquals(activiteTest.get(no), result);
    }

    @Test
    public void activiteFindAllTest() {
        EntityManager em = JpaUtil.obtenirEntityManagerTest();
        //Ajout
        for (int i = 0; i < remplissage; i++) {
            em.persist(activiteTest.get(i));
        }

        //Recherche
        ActiviteDAO aDAO = new ActiviteDAO();
        List<Activite> result = null;
        try {
            result = aDAO.findAll();
        } catch (Exception ex) {
            Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        boolean answer = true;

        for (int i = 0; i < remplissage; i++) {
            if (!result.contains(activiteTest.get(i))) {
                answer = false;
            }
        }

        assertTrue(answer);

        /* EN TEMPS NORMAL
        //Rangement
        Activite initial[] = new Activite[activiteTest.size()];
        Activite resultat[] = new Activite[result.size()];
        initial = activiteTest.toArray(initial);
        resultat = result.toArray(resultat);
        
        assertArrayEquals(initial, resultat);
        //Problème : On rajoute beaucoup de "bruit" avec le persistence.xml. 
        // Il faudrait donc rentrer manuellement les 30 différentes activités.
         */
    }

    //------------------------- ADHERENT -------------------------
    @Test
    public void adherentCreerTest() {
        AdherentDAO aDAO = new AdherentDAO();
        aDAO.creer(adherentsTest.get(0));
    }

    @Test
    public void adherentFindByIDTest() {
        //Ajout
        EntityManager em = JpaUtil.obtenirEntityManagerTest();
        int no = 1;
        em.persist(adherentsTest.get(no));
        //Recherche
        AdherentDAO aDAO = new AdherentDAO();
        Adherent result = null;
        try {
            result = aDAO.findById(adherentsTest.get(no).getId());
        } catch (Exception ex) {
            Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertEquals(adherentsTest.get(no), result);
    }

    @Test
    public void adherentFindAllTest() {
        EntityManager em = JpaUtil.obtenirEntityManagerTest();
        //Ajout
        for (int i = 0; i < remplissage; i++) {
            em.persist(adherentsTest.get(i));
        }

        //Recherche
        AdherentDAO aDAO = new AdherentDAO();
        List<Adherent> result = null;
        try {
            result = aDAO.findAll();
        } catch (Exception ex) {
            Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        boolean answer = true;

        for (int i = 0; i < remplissage; i++) {
            if (!result.contains(adherentsTest.get(i))) {
                answer = false;
            }
        }

        assertTrue(answer);
    }

    @Test
    public void adherentFindByMailTest() {
        AdherentDAO aDAO = new AdherentDAO();
        aDAO.creer(adherentsTest.get(0));
    }
    //------------------------- ACTIVITE -------------------------

    //------------------------- DEMANDE -------------------------
    @Test
    public void demandeCreerTest() {
        //Pour éviter erreurs avec l'activite
        ActiviteDAO aDAO = new ActiviteDAO();
        aDAO.creer(activiteTest.get(0));
        AdherentDAO adDAO = new AdherentDAO();
        adDAO.creer(adherentsTest.get(0));
        //test de l'ajout
        DemandeDAO dDAO = new DemandeDAO();
        dDAO.creer(demandeTest.get(0));
    }

    @Test
    public void demandeFindByIdTest() {
        //Paramètre
        int no = 1;
        //Pour éviter erreurs avec l'activite
        AdherentDAO adDAO = new AdherentDAO();
        adDAO.creer(adherentsTest.get(no));
        ActiviteDAO aDAO = new ActiviteDAO();
        aDAO.creer(activiteTest.get(no));
        //Ajout
        DemandeDAO dDAO = new DemandeDAO();
        dDAO.creer(demandeTest.get(no));
        //Recherche
        Demande result = null;
        try {
            result = dDAO.findById(demandeTest.get(no).getId());
        } catch (Exception ex) {
            Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertEquals(demandeTest.get(no), result);
    }

    @Test
    public void demandeFindAllTest() {
        //Pour éviter erreurs avec l'activite
        AdherentDAO adDAO = new AdherentDAO();
        ActiviteDAO aDAO = new ActiviteDAO();
        DemandeDAO dDAO = new DemandeDAO();

        //Ajout
        for (int i = 0; i < remplissage; i++) {
            adDAO.creer(adherentsTest.get(i));
            aDAO.creer(activiteTest.get(i));
            dDAO.creer(demandeTest.get(i));
        }

        //Recherche
        List<Demande> result = null;
        try {
            result = dDAO.findAll();
        } catch (Exception ex) {
            Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        boolean answer = true;

        for (int i = 0; i < remplissage; i++) {
            if (!result.contains(demandeTest.get(i))) {
                answer = false;
            }
        }

        assertTrue(answer);
    }

    @Test
    public void demandeFindSameTest() {

        //Génération de date
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        String dateGenerated = "01/06/2013";
        Date dTMP = null;
        try {
            dTMP = formatter.parse(dateGenerated);
        } catch (ParseException ex) {
            Logger.getLogger(DAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Créer deux demandes, similaires.
        Demande toPush = new Demande(dTMP, "matin", true, adherentsTest.get(3), activiteTest.get(2));
        Demande toFind = new Demande(dTMP, "matin", true, adherentsTest.get(4), activiteTest.get(2));

        DemandeDAO dDAO = new DemandeDAO();
        List<Demande> result = null;
        result = dDAO.findSame(toFind);

        assertNotNull(result);
    }
    //------------------------- EVENEMENT -------------------------
}
